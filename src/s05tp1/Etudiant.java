package s05tp1;

import java.time.LocalDate;

public class Etudiant {
	private Personne personne;
	private long nip;
	private String formation;
	private double moyenne;
	
	public Etudiant(){
		
	}
	
	public Etudiant(String nom, String prenom, String formation, LocalDate dateNaissance){
		this.personne = new Personne(nom, prenom, dateNaissance);
		this.formation = formation;
		this.moyenne = 0;
	}
	
	public Etudiant(String nom, String prenom, String formation, long nip, LocalDate dateNaissance){
		this(nom, prenom, formation, dateNaissance);
		this.nip = nip;
	}

	//Getters / Setters
	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	public long getNip() {
		return nip;
	}

	public void setNip(long nip) {
		this.nip = nip;
	}

	public String getFormation() {
		return formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public double getMoyenne() {
		return moyenne;
	}

	public void setMoyenne(double moyenne) {
		this.moyenne = moyenne;
	}

	//Délégations
	public String getNom() {
		return personne.getNom();
	}

	public void setNom(String nom) {
		personne.setNom(nom);
	}

	public String getPrenom() {
		return personne.getPrenom();
	}

	public void setPrenom(String prenom) {
		personne.setPrenom(prenom);
	}

	public LocalDate getDateNaissance() {
		return personne.getDateNaissance();
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		personne.setDateNaissance(dateNaissance);
	}

	public int getAge() {
		return personne.getAge();
	}

	public void setAge(int age) {
		personne.setAge(age);
	}

	public void getNbYears(LocalDate date) {
		personne.getNbYears(date);
	}

	@Override
	public String toString() {
		return "nip=" + nip + ", formation=" + formation + ", moyenne=" + moyenne + ", " +personne;
	}

	public int hashCode() {
		return personne.hashCode();
	}

	public boolean equals(Object obj) {
		return personne.equals(obj);
	}
}