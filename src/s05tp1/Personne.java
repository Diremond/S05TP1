package s05tp1;

import java.time.LocalDate;

public class Personne {
	private String nom;
	private String prenom;
	private LocalDate dateNaissance;
	private int age;
	
	public Personne(){
		
	}
	
	public Personne(String nom, String prenom, LocalDate dateNaissance){
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	boolean plusVieux(Personne personne){
		if(personne.dateNaissance.isBefore(this.dateNaissance))
			return false;
		else
			return true;
	}
	
	public void getNbYears(LocalDate date){
		this.age = LocalDate.now().getYear()-date.getYear();
	}
	
	public String toString(){
		return "nom : "+nom+" | pr�nom : "+prenom+" | age : "+age;
	}
}
