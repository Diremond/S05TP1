package s05tp1;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;

public class Promotion {
	private HashMap<String, EtudiantNoteAbs> etuNoteAbs;
	
	
	public Promotion(){
		etuNoteAbs = new HashMap<String, EtudiantNoteAbs>();
	}
	
	
	public String getMajor(){
		EtudiantNoteAbs mayorEtu = etuNoteAbs.get("Simon");
		for(EtudiantNoteAbs e : etuNoteAbs.values()){
			if(mayorEtu.getMoyenne() < e.getMoyenne()){
				mayorEtu = e;
			}
		}
		return "Voici le meilleur �l�ve de la promo : " + mayorEtu.getNom();
	}
	
	
	public String getAbsents(LocalDate date){
		String etuAbsents="";
		
		for(EtudiantNoteAbs e : etuNoteAbs.values()){
			if(e.getAbsences().indexOf(date) != -1)
				etuAbsents += e.getNom()+" ";
		}
		return "Personnes absentes durant ce jour : "+etuAbsents;
	}
	
	public String getIncurables(int max){
		String etuIncurables = "";
		for(EtudiantNoteAbs e : etuNoteAbs.values()){
			if(e.getAbsences().size() > max)
				etuIncurables += e.getNom()+" ";
		}
		return "Personnes incurables : "+etuIncurables;
	}

	public HashMap<String, EtudiantNoteAbs> getEtuNoteAbs() {
		return etuNoteAbs;
	}

	public void setEtuNoteAbs(HashMap<String, EtudiantNoteAbs> etuNoteAbs) {
		this.etuNoteAbs = etuNoteAbs;
	}
	
	public String toString(){
		String result = "";
		for(EtudiantNoteAbs e : etuNoteAbs.values()){
			result += e+" \n";
		}
		return result;
	}

	public void clear() {
		etuNoteAbs.clear();
	}

	public EtudiantNoteAbs get(Object key) {
		return etuNoteAbs.get(key);
	}

	public EtudiantNoteAbs put(String key, EtudiantNoteAbs value) {
		return etuNoteAbs.put(key, value);
	}

	public EtudiantNoteAbs remove(Object key) {
		return etuNoteAbs.remove(key);
	}

	public int size() {
		return etuNoteAbs.size();
	}

	public Collection<EtudiantNoteAbs> values() {
		return etuNoteAbs.values();
	}
}