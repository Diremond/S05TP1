package s05tp1;

import java.time.LocalDate;

public class EssaiEtudiantNote {
	public static void main(String args[]){
		EtudiantNote etu1 = new EtudiantNote("Hallart", "Simon", "STI", 12, LocalDate.of(1998, 8, 25));
		etu1.getNbYears(LocalDate.of(1998, 8, 25));
		etu1.addNote(14);
		etu1.addNote(15);
		System.out.println(etu1);
	}
}