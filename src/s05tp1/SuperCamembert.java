package s05tp1;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import tps.Camembert;

//D�bug essaiCamembert .put semble poser probl�me, on initialise camembert, mais on n'utilise pas les deleguates methods
public class SuperCamembert {
	private Camembert camembert;
	private Map<String,Float> valeursMap;
	private Map<String,Color> colorsMap;
	
	public SuperCamembert(String titre, String[] legende, float[] valeurs, Color[] couleurs){
		valeursMap = new HashMap<String,Float>();
		colorsMap = new HashMap<String, Color>();
		
		for(int cpt = 0; cpt < legende.length; cpt++){
			valeursMap.put(legende[cpt], valeurs[cpt]);
		}
		for(int cpt = 0; cpt < couleurs.length; cpt++){
			colorsMap.put(legende[cpt], couleurs[cpt]);
		}
		camembert = new Camembert(titre, valeursMap, colorsMap);
	}
	
	public SuperCamembert(String titre, HashMap<String, Float> valeurs){
		valeursMap = new HashMap<String,Float>();
		colorsMap = new HashMap<String, Color>();
		
		colorsMap.put("un", Color.PINK);
		colorsMap.put("deux", Color.PINK);
		colorsMap.put("trois", Color.RED);
		colorsMap.put("quatre", Color.red);
		camembert = new Camembert(titre, valeurs, colorsMap);
	}
	
	
	//D�l�guations	
	public boolean colorer(Map<String, Color> couleurs) {
		return camembert.colorer(couleurs);
	}

	public boolean renseigner(Map<String, Float> valeurs) {
		return camembert.renseigner(valeurs);
	}
	
	

	public boolean renseigner(String[] legende, float[] valeurs){
		if(legende.length == valeurs.length){
			for(int cpt = 0; cpt < legende.length; cpt++){
				valeursMap.put(legende[cpt], valeurs[cpt]);
			}
			return true;
		}
		return false;
	}
	
	public boolean colorer(String[] legende, Color[] couleurs){
		if(legende.length == couleurs.length){
			for(int cpt = 0; cpt < couleurs.length; cpt++){
				colorsMap.put(legende[cpt], couleurs[cpt]);
			}
			return true;
		}
		return false;
	}
}