package s05tp1;

import java.time.LocalDate;
import java.util.ArrayList;

public class EtudiantNote {
	private Etudiant etudiant;
	private ArrayList<Integer> notes = new ArrayList<Integer>();
	
	public EtudiantNote(){
		
	}
	
	public EtudiantNote(String nom, String prenom, String formation, LocalDate dateNaissance){
		etudiant = new Etudiant(nom, prenom, formation, dateNaissance);
	}
	
	public EtudiantNote(String nom, String prenom, String formation, long nip, LocalDate dateNaissance){
		etudiant = new Etudiant(nom, prenom, formation, nip, dateNaissance);
	}
	
	//Getters / Setters
	public Personne getPersonne() {
		return etudiant.getPersonne();
	}

	public void setPersonne(Personne personne) {
		etudiant.setPersonne(personne);
	}

	public long getNip() {
		return etudiant.getNip();
	}

	public void setNip(long nip) {
		etudiant.setNip(nip);
	}

	public String getFormation() {
		return etudiant.getFormation();
	}

	public void setFormation(String formation) {
		etudiant.setFormation(formation);
	}

	public double getMoyenne() {
		return etudiant.getMoyenne();
	}

	public void setMoyenne(double moyenne) {
		etudiant.setMoyenne(moyenne);
	}

	public String getNom() {
		return etudiant.getNom();
	}

	public void setNom(String nom) {
		etudiant.setNom(nom);
	}

	public String getPrenom() {
		return etudiant.getPrenom();
	}

	public void setPrenom(String prenom) {
		etudiant.setPrenom(prenom);
	}

	public LocalDate getDateNaissance() {
		return etudiant.getDateNaissance();
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		etudiant.setDateNaissance(dateNaissance);
	}

	public int getAge() {
		return etudiant.getAge();
	}

	public void setAge(int age) {
		etudiant.setAge(age);
	}

	public void getNbYears(LocalDate date) {
		etudiant.getNbYears(date);
	}

	public int hashCode() {
		return etudiant.hashCode();
	}

	public boolean equals(Object obj) {
		return etudiant.equals(obj);
	}

	//Getters / Setters
	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	@Override
	public String toString() {
		return etudiant + ", notes=" + notes;
	}

	public void addNote(int note){
		int total = 0;
		
		if(note >=0 && note<=20)
			notes.add(note);
		for(Integer i :notes){
			total = total + i;
			etudiant.setMoyenne((double)total / (double)notes.size());
		}
	}
}