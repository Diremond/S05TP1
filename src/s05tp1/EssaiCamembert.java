package s05tp1;

import java.awt.Color;
import java.util.HashMap;

public class EssaiCamembert {

	public static void main(String[] args) {
		Color[] colors = new Color[4];
		colors[0] = Color.orange;
		colors[1] = Color.pink;
		colors[2] = Color.red;
		colors[3] = Color.yellow;
		
		String[] legendes = new String[4];
		legendes[0] = "un";
		legendes[1] = "deux";
		legendes[2] = "trois";
		legendes[3] = "quatre";
		
		float[] valeurs = new float[4];
		valeurs[0] = 25;
		valeurs[1] = 25;
		valeurs[2] = 25;
		valeurs[3] = 25;
		
		HashMap<String, Float> mp = new HashMap<String, Float>();
		mp.put("un", 25f);
		mp.put("deux", 25f);
		mp.put("trois", 25f);
		mp.put("quatre", 25f);

		new SuperCamembert("Camembert", legendes, valeurs, colors);
		
		//Palette par d�faut
		new SuperCamembert("Camembert", mp);
	
	}

}
