package s05tp1;

import java.time.LocalDate;
import java.util.ArrayList;

public class EtudiantNoteAbs {
	private EtudiantNote etudiantNote;
	private ArrayList<LocalDate> absences = new ArrayList<LocalDate>();
	
	public EtudiantNoteAbs(){
		
	}
	
	public EtudiantNoteAbs(String nom, String prenom, String formation, LocalDate dateNaissance){
		etudiantNote = new EtudiantNote(nom, prenom, formation, dateNaissance);
	}

	public EtudiantNoteAbs(String nom, String prenom, String formation, long nip, LocalDate dateNaissance){
		etudiantNote = new EtudiantNote(nom, prenom, formation, nip, dateNaissance);
	}
	
	public Personne getPersonne() {
		return etudiantNote.getPersonne();
	}

	public void setPersonne(Personne personne) {
		etudiantNote.setPersonne(personne);
	}

	public long getNip() {
		return etudiantNote.getNip();
	}

	public void setNip(long nip) {
		etudiantNote.setNip(nip);
	}

	public String getFormation() {
		return etudiantNote.getFormation();
	}

	public void setFormation(String formation) {
		etudiantNote.setFormation(formation);
	}

	public double getMoyenne() {
		return etudiantNote.getMoyenne();
	}

	public void setMoyenne(double moyenne) {
		etudiantNote.setMoyenne(moyenne);
	}

	public String getNom() {
		return etudiantNote.getNom();
	}

	public void setNom(String nom) {
		etudiantNote.setNom(nom);
	}

	public String getPrenom() {
		return etudiantNote.getPrenom();
	}

	public void setPrenom(String prenom) {
		etudiantNote.setPrenom(prenom);
	}

	public LocalDate getDateNaissance() {
		return etudiantNote.getDateNaissance();
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		etudiantNote.setDateNaissance(dateNaissance);
	}

	public int getAge() {
		return etudiantNote.getAge();
	}

	public void setAge(int age) {
		etudiantNote.setAge(age);
	}

	public void getNbYears(LocalDate date) {
		etudiantNote.getNbYears(date);
	}

	public int hashCode() {
		return etudiantNote.hashCode();
	}

	public boolean equals(Object obj) {
		return etudiantNote.equals(obj);
	}

	public Etudiant getEtudiant() {
		return etudiantNote.getEtudiant();
	}

	public void setEtudiant(Etudiant etudiant) {
		etudiantNote.setEtudiant(etudiant);
	}
	
	public ArrayList<LocalDate> getAbsences() {
		return absences;
	}

	public void setAbsences(ArrayList<LocalDate> absences) {
		this.absences = absences;
	}

	public String toString() {
		return etudiantNote.toString()+", Nb d'absences= " + absences;
	}

	public void addNote(int note) {
		etudiantNote.addNote(note);
	}

	public EtudiantNote getEtudiantNote() {
		return etudiantNote;
	}

	public void setEtudiantNote(EtudiantNote etudiantNote) {
		this.etudiantNote = etudiantNote;
	}
	
	public void addAbs(LocalDate date){
		if(date.isBefore(LocalDate.now()))
			absences.add(date);
	}
	
	public void supprAbs(LocalDate date){
		absences.remove(date);
	}
	
	public static void main(String[] args){
		EtudiantNoteAbs etu1 = new EtudiantNoteAbs("Hallart", "Simon", "STI", 12, LocalDate.of(1998, 8, 25));
		etu1.addAbs(LocalDate.of(2017, 3, 2));
		etu1.addAbs(LocalDate.of(2017, 2, 2));
		etu1.getNbYears(LocalDate.of(1998, 8, 25));
		etu1.addNote(14);
		etu1.addNote(15);
		System.out.print(etu1);
	}
}
