package s05tp1;

import java.time.LocalDate;

public class EssaiPromotion {

	public static void main(String[] args) {
		Promotion p = new Promotion();
		
		EtudiantNoteAbs simon = new EtudiantNoteAbs("Hallart", "Simon", "STI", 59, LocalDate.of(1998, 8, 25));
		EtudiantNoteAbs personne2 = new EtudiantNoteAbs("Personne", "Deux", "S", 59, LocalDate.of(1997, 6, 16));
		
		p.put("Simon", simon);
		p.put("Personne2", personne2);
		
		simon.addAbs(LocalDate.of(1998, 2, 10));
		simon.addAbs(LocalDate.of(1998, 3, 10));
		simon.addAbs(LocalDate.of(1998, 2, 11));
		simon.addNote(10);
		simon.addNote(11);
		
		personne2.addAbs(LocalDate.of(1997, 8, 5));
		personne2.addAbs(LocalDate.of(1998, 2, 11));
		personne2.addNote(12);
		personne2.addNote(13);
		
		System.out.print(p);
		System.out.println(p.getAbsents(LocalDate.of(1998, 2, 11)));
		System.out.println(p.getMajor());
		System.out.println(p.getIncurables(2));
		
	}

}
